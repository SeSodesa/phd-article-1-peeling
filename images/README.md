# Images

Images are to be stored here. If an image is generated with TikZ, it should
have its own named subfolder with `main.tex` as the image source file name.
