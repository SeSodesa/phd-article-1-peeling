# Article 1

Outlier analysis in a high-resolution FE mesh with a thin cortical layer.
Compile by either first compiling all of the individual TikZ images under the
folder `images/` and then the main document, or by using the attached Python
program `compile_tex.py` with

	python3 compile_tex.py /path/to/this/folder

to automatically first compile the images and then the main document. When the
PDF images exist, one can compile the document with

	compiler main.tex && bibtex main && compiler main.tex && compiler main.tex

where `compiler` ∈ {`pdflatex`,`lualatex`}, to make sure that all cross
references and citations are generated correctly.

# Notable files and folders

### [metadata.tex](./metadata.tex)

Author and document information are to be added here as macro definitions.

### [preamble.tex](./preamble.tex)

Own packages are to be included and commands defined here.

### [main.tex](./main.tex)

This is the file that provides a context for the compilation process. In other
words, the commands

	compiler main.tex && compiler main.tex ,

where `compiler` ∈ {`pdflatex`, `lualatex`} are to be used to compile the
document.

### [sections/](./sections/)

Contains a file for each section in the article. This separation makes the
writing project more modular, and suitable for use with Git and Overleaf.

## License and copyright

Copyright © 2022–∞ Santtu Söderholm

See the attached `LICENSE` file for information on the licensing of this
project.
