\section{Discussion}\label{sec:discussion}

\EEG\ source modelling is often performed on spherical head
models~\cite{ary-etal-1981}\cite{roth-1993}\cite{meneghini--etal-2010}, or
head models that are realistically shaped, but which only contain \num 3 or
\num 4 brain compartments, such as gray matter, skull, skin and maybe the
cerebrospinal fluid layers~\cite{vorwerk-2014}. In addition, instead of these
models being volumetric, only boundaries of the modelled brain compartments
might be included, if forward modelling is based on boundary element
methods~\cite{hamalainen-etal-1993}\cite{kybic--etal-2005}. Those are less
resource-intensive than finite element methods, but also do not model the
volumetric aspects of the problem as accurately.

In a realistic situation, there are at least 19 different brain tissue types,
and this is only if one does not take into account the inhomogeneities within
these major compartments, such as different types of bone in the
skull~\cite{dannhauer-2011}. Each compartment can have its own isotropic or
anisotropic conductivity structure, which affects the forward solution \(L\).
Simplified approaches thus have little hope of modelling the subtleties
introduced by a realistic head model~\cite{meneghini--etal-2010}. Therefore in
this paper, we performed our investigations in a realistically shaped
high-resolution finite element head model~\cite{duneuro-head-model}, with
\num{19} major brain compartments present. Most importantly, the modelled
brain activity was restricted to realistically shaped and volumetrically
refined compartments of gray matter, where especially the cortical portion is
only a few \si{\milli\meter} thick. This again required (as one option) the
use of the tightly supported \(\Hdiv\) source model, which allowed the
placement of dipoles with nonzero lengths into these thin gray matter layers.

In this setting, we investigated how restricting tetrahedra, into which a set
of \(\Hdiv\) dipoles \(\Jp = \Jp\arcs{\vec x}\) might be placed, affects the
source localisation error \(\Delta\) of \sLORETA\ and \DipoleScan. The
restriction was based on disallowing the placement of dipoles near surfaces of
active layers. The relevance of this study comes from the well-known fact,
that the convergence of the forward \EEG\ solution is negatively affected,
when sources are placed near conductivity discontinuities at said tissue
boundaries. This applies both to boundary element
methods~\cite{he-1999}\cite{fuchs-2001}, and different finite element
approaches, such as the Subtraction Method~\cite{wolters--etal-2007} and the
\(\Hdiv\) approach~\cite{pursiainen--vorwerk--wolters-2016}, which was used in
this paper. This again negatively affects the localisation of synthetic
sources via different inverse methods.

Before presenting the above results, we observed how the peeling algorithm
works. In addition to this, our numerical forward solver was compared to a
semi-analytical one, to ensure that it works appropriately in a simplified
spherical domain and at higher eccentricities, where cortical sources are
known to reside, but where the errors are also known to be more extensive.

The peeling algorithm, which restricts the positions \(\vec x\) where sources
can be placed, was found to function as intended: it recognized the intended
tetrahedra and always removed at least one layer from the surfaces of the
specified brain layers, to prevent singularities from appearing in the forward
solution because of discontinuities or jumps in conductivities between
neighbouring source tetra. The only anomaly related to the source positioning
after peeling was that the \(d\subt p = \SI{0.5}{\milli\meter}\) case seemed
to contain \emph{more} sources than the \(d\subt p = \SI{0}{\milli\meter}\)
case. This can be explained with \zi's iterative way of evenly distributing
sources into the active volume, if the initial guess is not near the
user-given amount.

Comparing the numerical lead field or forward solution \(L\) to that of the
semi-analytical \(3\)-layer Ary model~\cite{ary-etal-1981} produced
appropriately good results, even at above \SI{98}{\percent} eccentricities,
with upper \SI{75}{\percent} quantiles of magnitude measure~(\MAG) and relative
difference measure~(\RDM) being at most \num{0.06} and \num{0.04},
respectively. It was not obvious, whether a more refined mesh would diminish
differences between the semi-analytical and numerical solutions \(\La\) and
\(\Ln\). In fact, having a more refined mesh seems to produce deteriorated
median results in the case of \MAG, whereas with \RDM\ the outcome is slightly
improved. This was especially true when \PBO~\cite{bauer--etal-2015} was used
for synthetic source interpolation, and hence it was chosen as the optimization
method in the case of the realistic head model. \MPO~\cite{atena-2021} was also
considered, but was deemed less suitable, as according
to~\cite{pursiainen--vorwerk--wolters-2016} \MPO\ is less stable than \PBO.

% it performed worse when measuring differences with
% \RDM, which captures the connections between forward and inverse
% errors better than~\MAG.

When looking at the average localisation errors \(\Delta\), and comparing the
results to those of Cuffin et. al.~\cite{cuffin-2001}, one can observe that
the performance of \sLORETA\ is inferior to that of \DipoleScan. The reported
\(\Delta < \SI{10.5}{\milli\meter}\) is only reached at lower noise levels,
when \(\SNR\geq\SI{30}{\decibel}\), whereas with \DipoleScan\ an average
within acceptable boundaries can be obtained all the way down to \(\SNR =
\SI{15}{\decibel}\). In the case of \sLORETA, the peeling cannot be said to
improve the average localisation accuracy, as utilizing it seems to first
improve the result when going from a peeling depth \(\peeld =
\SI{0}{\milli\meter}\) to \(\peeld = \SI{0.5}{\milli\meter}\), but then
localisation accuracy is decreased again, when \(\peeld\) is increased to
\SI{1}{\milli\meter}. Only at the very highest \SNR\ can a slight but
systematic decrease in the average \(\Delta\) be seen. \DipoleScan\ is
slightly more consistent in its performance with respect to peeling, as with
\(\SNR\geq\SI{20}{\decibel}\) the added peeling no longer increases the
average \(\Delta\). It should be noted, that Cuffin used a 3-compartment
boundary element model with a simplex search method to locate
sources~\cite{press-1989}, and hence our results are not directly comparable.

The behaviour of localisation error outliers also reflect the above state of
matters. In the case of \sLORETA, the initial impression is that peeling seems
to reduce the general denseness of the outliers clouds further above the box
plots, but at the same time some maximum outliers are further away from the
tops of the whiskers, especially in the case of \(d\subt p =
\SI{0.5}{\milli\meter}\). Peeling somewhat increases the number of outliers
for the inverse method at higher \SNR, although with \(\SNR <
\SI{20}{\decibel}\) there seem to be cases where the outliers are reduced with
added peeling. A possible explanation for this is that peeling reduces
statistical variability in the low-\SNR\ cases of \sLORETA. \DipoleScan\ again
performs more consistently, with the numbers of outliers being reduced at
every \(\SNR\geq\SI{15}{\decibel}\), when the peeling depth \(d\subt p\) is
increased. It might then be said that for \DipoleScan, the effects of random
noise seem to dominate when \SNR\ is reduced beyond this point.

When observing the localisation error as a function of primary dipole
position, \(\Delta = \Delta\arcs{\vec x}\) of \sLORETA\ and \DipoleScan, the
superior performance of the latter becomes obvious. With \sLORETA, while the
actual scale of the localisation error is reduced as \SNR\ increases, we end
up having more outliers with a relatively large \(\Delta\) in the entire
volume. In contrast, the behaviour of \DipoleScan\ is again more consistent:
not only does the localisation error go down with increasing \SNR, as one
would expect, the localisation error outliers become more focused into the
deep structures of the brain, where the sources are further away from the
sensors. The benefits of increasing the peeling depth \(\peeld\) are also not
unambiguous in either case. It seems that especially in the case of \sLORETA,
increasing \(\peeld\) simply moves the positions where outliers occur around
the volume, instead of eliminating them. There are still places where the
areas containing larger errors become less focal, such as in the parietal
region at \(\SNR = \SI{25}{\decibel}\). The fluctuation of the higher
localisation error patches around the volume is also visible in the case of
\DipoleScan, although maybe to a lesser extent. The consistent reduction in
\(\Delta\) with increased peeling seems to be slightly more prominent, as can
be seen in the environment of \emph{corpus callosum} and around the lower part
of the frontal lobe at \SNR\ between \num{20}--\SI{30}{\decibel}. The takeaway
from this is, that peeling the active brain layers does not guarantee
unambiguously better localisation results, but that it might do so on a
regional and \SNR\ basis.

A look at the relative strengths of reconstructions at
\(\SNR=\SI{30}{\decibel}\) also mostly supports this interpretation from the
point of view of varying the peeling depth \(\peeld\), although there seems to
be a hope of seeing an actual improvement in the case of \DipoleScan\ and a
normal source, which is the ideal case for \EEG: we can see the reconstruction
becoming slightly more focal around the plotted source. With \sLORETA, the
peeling seems to increase the general strength of the reconstruction around
the tangential source, whereas in the case of a normal source we see a
\emph{very} slight shift of the local peak of the distribution \(\rec\Jp\)
towards the source itself.

The dispersion measures of the results of different inverse methods were seen
to be improved, with parts of regions of largest dispersions seeing a
reduction of \SI{1.0}{\milli\meter} in the width of the peak around a
reconstructed dipole position. This is yet another indication of the
reconstruction becoming more focal in those regions, as a reduction in
dispersion indicates, that the amplitude of \(\rec\Jp\) is reduced in the
\SI{30}{\milli\meter} ROI for the dispersion around each respective source
position, with the most focal point being at the center of the ROI.
