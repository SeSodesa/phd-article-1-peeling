\section{Finite Element Integrals}\label{app:A}

The finite element forward solution is constructed from the Poisson equations,
derived from the quasi-static or almost time-invariant in-volume
approximations of Maxwell's equations, by taking their
divergences~\cite{sarvas-1987}:
\begin{align*}
	\divp\Jt &= 0 \,, \addnumber{eq:Jtot-constraint} \\
	\divp\Jp &= \divp\arcs{\sigma\nabla u} \addnumber{eq:Jp-constraint}
\end{align*}
with the surface constraints~\cite{atena-2021}
\begin{align*}
	\arcs{\sigma\nabla u}\cdot\vec n &= 0 \,.
	\addnumber{eq:surface-constraint}
\end{align*}
Here  \(\Jt = \Jp + \Jv\) describes the total current density or the sum of
in-axon and out-of-axon currents \(\Jp\) and \(\Jv\), \(\sigma\) being the
position-dependent conductivity tensor and \(\vec n\) denoting a surface
normal of the volume.

The finite element solution is then constructed by discretizing the domain
\(\domain\) into nodes \(n_i\) and elements, and assigning normalized and linearly
interpolated nodal basis functions
\begin{align*}
	\psi_j
	&=
	\left\{
		\begin{aligned}
			1 &\,, \quad  i = j \\
			0 &\,, \quad  i \ne j
		\end{aligned}
	\right.
	\addnumber{eq:nodal-basis-u}
	\\
	\vec w_j
	&=
	\left\{
		\begin{aligned}
			\frac {(1,1,1)}{\norm{(1,1,1)}} &\,, \quad i = j \\
			(0,0,0) &\,, \quad i \ne j
		\end{aligned}
	\right.
	\addnumber{eq:nodal-basis-Jp}
\end{align*}
with finite support to each node \(n_i\) in the finite element mesh. The
potential \(u\) and current density \(\Jp\) are then discretized as
\begin{align*}
	u \approx\discr u &= \sum_i x_i \psi_i \addnumber{eq:discretized-u}\\
	\Jp\approx\discr\Jp &= \sum_j z_j \vec w_j \addnumber{eq:discretized-Jp}
\end{align*}
Now, taking the right hand side of equation~\eqref{eq:Jp-constraint}, scaling
it with a weight function \(v\) and partially integrating over the volume
\(\domain\), we get
\begin{align*}
	\begin{gathered}
		\nint [\domain] {v \arcs{ \divp\arcs{ \sigma\nabla u } } } \domain \\
		\veq \\
		\nint [\domain] { \divp\arcs{ v\sigma\nabla u } } \domain
		-
		\nint [\domain] { \arcs{ \arcs{ \sigma\nabla u } \cdot \nabla } v } \domain \,,
	\end{gathered}
\end{align*}
where the minuend can be written in another form via the divergence theorem,
causing it to be nullified because of equation~\eqref{eq:surface-constraint}:
\begin{equation*}
	\begin{gathered}
		\nint [\domain] {v \arcs{ \divp\arcs{ \sigma\nabla u } } } \domain \\
		\veq \\
		\nint [\surface \domain] { v \arcs{ \sigma\nabla u } \cdot \vec n }*{ \surface \domain }
		-
		\nint [\domain] { \arcs{ \sigma\nabla u } \cdot \nabla v } \domain \\
		\veq \\
		-
		\nint [\domain] {  \nabla v \cdot \arcs{ \sigma\nabla u } } \domain \\
	\end{gathered}
\end{equation*}
Hence with the help of equation~\eqref{eq:Jp-constraint} we can write
\begin{equation}\label{eq:weak-solution}
		\nint [\domain] {v \arcs{ \divp\Jp } } \domain
		=
		- \nint [\domain] {  \nabla v \cdot \arcs{ \sigma\nabla u } } \domain
\end{equation}

Replacing \(u\) with \eqref{eq:discretized-u} and \(\Jp\) with
\eqref{eq:discretized-Jp}, we have
\begin{align*}
	\begin{gathered}
		-\nint [\domain] {v\divp\arcs*{\sum_j z_j \vec w_j}} \domain \\
		\shortparallel \\
		\nint [\domain] {\nabla v\cdot\arcs*{\sigma\nabla\arcs*{\sum_i x_i \psi_i}}} \domain
	\end{gathered}
\end{align*}
Choosing the weight functions \(v\) from the set of basis functions \(\psi\),
this is further transformed into
\begin{align*}
	\begin{gathered}
		\nint [\domain] {\psi_i\divp\arcs*{\sum_j z_j \vec w_j}} \domain \\
		\shortparallel \\
		\nint [\domain] {\nabla\psi_j\cdot\arcs*{\sigma\nabla\arcs*{\sum_i x_i \psi_i}}} \domain
	\end{gathered}
\end{align*}
By linearity of integration and differentiation, we can take the sum out of
the integral, and get
\begin{align*}
	\begin{gathered}
		\sum_j z_j \nint [\domain] {\psi_i\arcs{\divp\vec w_j}} \domain
			= \sum_j G\subi{i,j} z_j \\
		\shortparallel \\
		\sum_i x_i \nint [\domain] {\nabla\psi_j\cdot\arcs{\sigma\nabla\psi_i}} \domain
			= \sum_i A\subi{i,j} x_i
	\end{gathered}
\end{align*}

From here we see that the equation can be expressed as a matrix product
\begin{equation}\label{eq:maxwell-matrix-equation}
	A\vec x = G\vec z\,,
\end{equation}
where matrix \(A\in\Rset^{N\times N}\) is known as the \emph{stiffness matrix}
of the finite element mesh. The product \(G\vec z = \vec f\) is the so-called \emph{load
vector}, which is system-specific, here representing the activity in the
brain. It can be shown to follow the equality
\begin{equation}
	G\subi{\psi,\vec w} \vec z\subi{\vec w}
	= \frac {
		s\subi{\set{\psi,n_j}} - s\subi{\set{\psi,n_i}}
	}{
		\norm {\vec x\subi{n_j} - \vec x\subi{n_i}}
	} \vec z\subi{\vec w}
\end{equation}
with \(s\subi{\set{\psi,n}} = 1\), if the basis function \(\psi\) is supported
around node \(n\) and zero otherwise.

With these formulations in place, measurements at the electrodes \(\vec y\) can be
computed as~\cite{pursiainen--vorwerk--wolters-2016}\cite{miinalainen-2019}
\begin{equation}\label{eq:appendix-lead-field}
	\vec y = R \vec x = R \inverse A G \vec z = T G \vec z = T\vec f \,,
\end{equation}
where the matrix \(R\) is used to set the zero potential level of the system
and therefore choose a unique
solution~\cite{pursiainen--vorwerk--wolters-2016}, and \(T\) is a transfer
matrix.
