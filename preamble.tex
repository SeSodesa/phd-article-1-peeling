%%% Packages

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{mathtools} % patched amsmath
\ifluatex
	\usepackage{fourier-otf}
\else
	\usepackage{amssymb}
	\usepackage{fourier}
\fi
\usepackage{xparse}
\usepackage{siunitx}
\usepackage[labelfont=bf]{caption}
\usepackage[table]{xcolor}
\usepackage{booktabs}
\usepackage{pgfmath}
\usepackage{xfp}

%%% Commands with xparse

%% Words one does not wish to misspell.

\def\Matlab{Matlab}
\def\zi{Zeffiro Interface}
\def\ZI{ZI}
\def\fe{finite element}
\def\FE{FE}
\def\fem{finite element method}
\def\FEM{FEM}
\def\PEM{PEM}
\def\pem{point electrode model}
\def\CEM{CEM}
\def\cem{complete electrode model}
\def\eeg{electroencephalography}
\def\EEG{EEG}
\def\meg{magnetoencephalography}
\def\MEG{MEG}
\def\pbo{Position-Based Optimization}
\def\PBO{PBO}
\def\mpo{Mean Position and Orientation}
\def\MPO{MPO}
\def\pcg{Pre-conditioned Conjugate Gradient}
\def\PCG{PCG}
\def\sLORETA{sLORETA}
\def\DipoleScan{Dipole Scan}
\def\csf{cerebrospinal fluid}
\def\CSF{CSF}
\def\mri{magnetic resonance imaging}
\def\MRI{MRI}
\def\ct{computer tomography}
\def\CT{CT}
\def\lcmv{linearly constrained minimum variance}
\def\LCMV{LCMV}
\def\Beamformer{Beamformer}
\def\SNR{\text{SNR}}

%%% Scalable delimiters with mathtools

\DeclarePairedDelimiter{\arcs}{(}{)}
\DeclarePairedDelimiter{\squares}{[}{]}
\DeclarePairedDelimiter{\waves}{\{}{\}}
\DeclarePairedDelimiter{\set}{\{}{\}}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}
\DeclarePairedDelimiter\angles{\langle}{\rangle}
\DeclarePairedDelimiter\qunit{[}{]}
\DeclarePairedDelimiter\abs{|}{|}

%% Notations

\NewDocumentCommand\Rset{}{\mathbb R}

\NewDocumentCommand\surface{m}{\partial #1}

% Redefine \vec.

\RenewDocumentCommand\vec{m}{\mathbf{#1}}

% Vertical equals sign.

\NewDocumentCommand\veq{}{\shortparallel}

% A shorthand for a differential d

\DeclareMathOperator{\diff}{d}
\DeclareMathOperator{\pdiff}{\partial}

% Total and partial derivatives.

\NewDocumentCommand\tderiv{mm}{\frac{\diff #1}{\diff #2}}

\NewDocumentCommand\pderiv{mm}{\frac{\pdiff #1}{\pdiff #2}}

% Curl and divergence prefixes.

\NewDocumentCommand\divp{}{\nabla\cdot}

\NewDocumentCommand\curlp{}{\nabla\times}

% Sub- and superindices.

\NewDocumentCommand\subi{m}{_{#1}}
\NewDocumentCommand\supi{m}{^{#1}}

\NewDocumentCommand\subt{m}{_{\mathrm{#1}}}
\NewDocumentCommand\supt{m}{^{\mathrm{#1}}}

\NewDocumentCommand\discr{m}{{#1}\subt{d}}

% Primary current distribution.

\NewDocumentCommand\Jp{}{\vec J\subi{\mathrm p}}

\NewDocumentCommand\Jv{}{\vec J\subi{\mathrm v}}

\NewDocumentCommand\Jt{}{\vec J\subi{\mathrm{tot}}}

\NewDocumentCommand\tetrahedralization{}{\mathcal{T}}
\NewDocumentCommand\modelerror{}{E_{\tetrahedralization + \Jp}}
\NewDocumentCommand\measurementerror{}{E_{M}}
\NewDocumentCommand\inverseerror{}{E_{K}}

\NewDocumentCommand\est{sm}{%
	\IfBooleanTF{#1}{%
		\angles*{#2}%
	}{%
		\angles{#2}%
	}%
}

\NewDocumentCommand\rec{sm}{%
	\IfBooleanTF{#1}{%
		\arcs*{#2}^\wedge%
	}{%
		#2^\wedge%
	}%
}

\NewDocumentCommand\Hdiv{}{H(\text{div})}

% Schur complement.

\NewDocumentCommand\schurc{m}{{#1}^{\complement}}

% MAG and RDM commands.

\NewDocumentCommand\MAG{}{MAG}

\NewDocumentCommand\RDM{}{RDM}

%% \nint
%
% An normal integral command. Usage: \nint[l][u]{i}{v}, where l
% is an optional lower bound, u is an optional upper bound, i is
% the integrand and v the variable of integration.

\NewDocumentCommand\nint{O{}O{}msm}{%
    \int_{#1}^{#2} #3\,\diff%
	\IfBooleanTF{#4}{\arcs*{#5}}{#5}%
}

%% \pint
%
% A partial integral command. Usage: \pint[l][u]{i}{v}, where l
% is an optional lower bound, u is an optional upper bound, i is
% the integrand and v the variable of integration.

\NewDocumentCommand\pint{O{}O{}msm}{%
    \int_{#1}^{#2} #3\,\pdiff%
	\IfBooleanTF{#4}{\arcs*{#5}}{#5}%
}

% Restriction operator

\NewDocumentCommand\restr{smm}{%
	\IfBooleanTF{#1}{%
		\arcs*{#2\mid#3}%
	}{%
		\arcs{#2\mid#3}%
	}%
}

% Probability distribution command

\NewDocumentCommand\pdist{sm}{%
	\IfBooleanTF{#1}{%
		p\arcs*{#2}%
	}{%
		p\arcs{#2}%
	}%
}

% Least squares dagger command.

\NewDocumentCommand\inverse{sm}{%
	\IfBooleanTF{#1}{%
		\arcs*{#2}^{-1}%
	}{%
		#2^{-1}%
	}%
}

% Least squares dagger command.

\NewDocumentCommand\pinverse{sm}{%
	\IfBooleanTF{#1}{%
		\arcs*{#2}^\dagger%
	}{%
		#2^\dagger%
	}%
}

% Transpose command.

\NewDocumentCommand\transpose{sm}{%
	\IfBooleanTF{#1}{%
		\arcs*{#2}^{\mathrm T}%
	}{%
		#2^{\mathrm T}%
	}%
}

% Regularization command.

\NewDocumentCommand\regularization{sm}{%
	\IfBooleanTF{#1}{%
		\arcs*{#2}^\ddagger%
	}{%
		#2^\ddagger%
	}%
}


% A vector of ones.

\NewDocumentCommand\onesvec{}{\mathbf{1}}

% An n-norm command.

\NewDocumentCommand\nnorm{smm}{%
	\IfBooleanTF{#1}{%
		\norm*{#2}_{#3}%
	}{%
		\norm{#2}_{#3}%
	}%
}

% Argmin and -max commands.

\DeclareMathOperator*\argmin{arg\,min}
\DeclareMathOperator*\argmax{arg\,max}
\DeclareMathOperator*\indmin{ind\,min}
\DeclareMathOperator*\indmax{ind\,max}

% Inverse and spatial dispersion

\DeclareMathOperator*\inv{inv}
\DeclareMathOperator*\SD{SD}

% Trace.

\NewDocumentCommand\trace{sm}{%
	\IfBooleanTF{#1}{%
		\operatorname{trace}\arcs*{#2}%
	}{%
		\operatorname{trace}#2%
	}%
}

\DeclareMathOperator\var{Var}
\DeclareMathOperator\cov{Cov}

% A beamformer filter matrix.

\NewDocumentCommand\bff{}{W}

% Numerical and analytical lead fields.

\NewDocumentCommand\La{}{L\subt{a}}

\NewDocumentCommand\Ln{}{L\subt{n}}

% Relative residual variance and goodness of fit.

\NewDocumentCommand\rrv{sm}{%
	\IfBooleanTF{#1}{%
		\operatorname{RRV}\arcs*{#2}%
	}{%
		\operatorname{RRV}#2%
	}%
}

\NewDocumentCommand\gof{sm}{%
	\IfBooleanTF{#1}{%
		\operatorname{GoF}\arcs*{#2}%
	}{%
		\operatorname{GoF}#2%
	}%
}

\NewDocumentCommand\tsvd{}{truncated singular value decomposition}
\NewDocumentCommand\tSVD{}{tSVD}

\NewDocumentCommand\source{smmm}{%
	\IfBooleanTF{#1}{%
		\arcs*{#2, #3, #4}%
	}{%
		\arcs{#2, #3, #4}%
	}%
}

\DeclareMathOperator\mMAG{MAG}

\DeclareMathOperator\mRDM{RDM}

\NewDocumentCommand\domain{}{\Omega}

% Peeling depth

\NewDocumentCommand\peeld{}{d\subt{p}}

%% Reference commands

\NewDocumentCommand\figref{m}{Figure~\ref{#1}}

\NewDocumentCommand\tabref{m}{Table~\ref{#1}}

\NewDocumentCommand\appref{m}{\ref{#1}}

%%% Heat-mapped table cells (requires package calc).
%
%     \hmcell{current numerical value}{maximum numerical value}
%
% This command takes two arguments, the current color and the maximum color in
% the table. It the scales the color to find out where it lies in the range
% between 0 and this maximum value.
%
% For the scaling, this macro relies on the calc package. The cell coloring
% code is generated by the table module of the package xcolor. The package
% siunitx is used to render the given first argument.
%

\NewDocumentCommand\colorhmportion{}{50}

\NewDocumentCommand\hmcell{mm}{%
	\def\division{\fpeval{#1 / #2}}%
	\def\totalpercentage{\fpeval{100 * \division}}%
	\ifdim\totalpercentage pt < 50 pt%
		\def\mycolor{blue}%
		\def\colorpercentage{\fpeval{\colorhmportion - \colorhmportion * \division}}%
	\else%
		\def\mycolor{red}%
		\def\colorpercentage{\fpeval{\colorhmportion * \division}}%
	\fi%
	\edef\x{\noexpand\cellcolor{\mycolor!\colorpercentage!white}}\x%
	\num{#1}%
}

%%% Other commands

% A command for monospaced text.

\NewDocumentCommand\code{m}{\texttt{#1}}

% Renew figure environment to prevent it from floating.
%
% Example usage:
%
% \begin{figure}
% \includegraphics[width=\linewidth]{path/to/image/file}
% \end{figure}

\NewDocumentEnvironment{myfigure}{O{\textwidth}mm}{%
	\par%
	\vspace{1ex}%
	\refstepcounter{figure}%
	\begin{minipage}[t]{#1}%
	\captionsetup{type=figure}%
	\centering%
}{%
	\addtocounter{figure}{-1}%
	\captionof{figure}{#2}%
	\label{#3}%
	\end{minipage}%
%	\par%
%	\vspace{1ex}%
}

%% Define counters if they do not already exist.

\makeatletter
\ifx\c@figure\undefined
	\newcounter{figure}
\fi
\ifx\c@section\undefined
	\newcounter{section}
\fi
\ifx\c@table\undefined
	\newcounter{table}
\fi
\makeatother

\numberwithin{figure}{section}

% Subfigures follow the same logic as figures.

\DeclareCaptionType{subfigure}

\DeclareCaptionLabelFormat{subfigure}{\textbf{(#2)}}

\captionsetup[subfigure]{
	labelformat=subfigure,
	font=scriptsize,
	justification=raggedright,
	%singlelinecheck=off
}

\RenewDocumentEnvironment{subfigure}{O{\textwidth}mm}{%
	\begin{minipage}[t]{#1}%
	\captionsetup{type=figure}%
	\centering%
}{%
	\captionof{subfigure}{#2}%
	\label{#3}%
	\end{minipage}%
}

\renewcommand{\thesubfigure}{\alph{subfigure}}

\NewDocumentCommand\subfigref{m}{\textbf{(\ref{#1})}}

%\numberwithin{subfigure}{figure}

% Renew table environment to prevent it from floating.
%
% Example usage:
%
% \begin{table}
% \begin{tabular}{column spec}
%     (table contents)
% \end{tabular}
% \end{table}

% \RenewDocumentEnvironment{table}{O{\textwidth}mm}{%
% 	\begin{minipage}[b]{#1}%
% 	\centering%
% 	\captionsetup{type=table}%
% 	\captionof{table}{#2}%
% 	\label{#3}%
% }{%
% 	\end{minipage}%
% }

% Set equation and table numbers.

\numberwithin{equation}{section}
\counterwithin{table}{section}

% A referentiable list item.

\NewDocumentCommand\refitem{m}{\item\label{#1}}

% For numbering a single line in aligned*

\NewDocumentCommand\addnumber{m}{\addtocounter{equation}{1}\tag{\theequation}\label{#1}}

%%% Generate PDF/A file

\begin{filecontents*}[overwrite]{\jobname.xmpdata}
\Title{\mytitle}
\Author{\myauthors}
\Language{en-GB}
\Subject{\mysubject}
\Keywords{\mykeywords}
\end{filecontents*}

\RequirePackage[a-3u,mathxmp]{pdfx}[2021/12/31]
\RequirePackage{url}
\hypersetup{hidelinks}

% Set subcounter after pdfx loads hyperref, so that cross-referencing works
% with subfigures.

\counterwithin*{subfigure}{figure}

\raggedbottom
